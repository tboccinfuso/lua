-- generate random numbers to play with
repeat
  local min = math.ceil(math.random(1, 9))
  local max = math.ceil(math.random(min, 10))
  local winningNumber = math.ceil(math.random(min, max))

  io.write(string.format("Guess a number between: %i and %i: ", min, max))

  guess = io.read()

  if tonumber(guess) == winningNumber then print("\nYou Win!") end

until tonumber(guess) == winningNumber
