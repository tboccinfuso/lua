-- create new file and overwrite to it
local file = io.open("./files/newFile.lua", "w+")

-- write to file
file:write("-- I was generated via the main.lua script!")

file:close()

-------------------------------------------------

-- append to the file
file = io.open("./files/newFile.lua", "a+")

file:write("\nThis text was appended!")

-- point to file start and read
file:seek("set", 0)
print(file:read("*a"))

file:close()

------------------------------------------------
local answer

print("\nNow let's get some input.\n")

io.write("Contine with this program (y/n)? ")

answer=io.read()

if answer=="y" then
  print("\n")

  io.write("What is your name? ")
  answer=io.read()

  io.write(string.format( "Hello, %s!",answer ))
end