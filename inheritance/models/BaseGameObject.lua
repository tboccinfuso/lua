-- A Main Base Game Object class that we will build off.
BaseGameObject = {name = '', rarity = '', type =''}

function BaseGameObject:Init(object)
  object = object or {}
  setmetatable(object, self)
  self.__index = self

  return object
end

function BaseGameObject:New(name, rarity, type)
  self.name = name
  self.rarity = rarity
  self.type = type

  return self
end

function BaseGameObject:DisplayRarity()
  local rarityToString = ""

  if self.rarity == 1 then
    rarityToString = "Common"
  elseif self.rarity == 2 then
    rarityToString = "Uncommon"
  elseif self.rarity == 3 then
    rarityToString = "Rare"
  else
    print("Unknown rarity type")
    return
  end

  print(string.format("The rarity of %s is: %s", self.name, rarityToString))
end

return BaseGameObject