BaseGameObject = require("../models/BaseGameObject")

Sword = BaseGameObject:Init()

function Sword:New(name, rarity, type, stats)
  self.name = name
  self.rarity = rarity
  self.type = type
  self.stats = stats or {}

  return self
end

function Sword:Attack()
  print(string.format("You were attacked with a %s, dealing %i damage. Ouch!", self.name, self.stats["damage"]))
end

return Sword