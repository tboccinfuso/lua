BaseGameObject = require("./models/BaseGameObject")
Sword = require("./models/Sword")

-- Init our new class objects
bgo = BaseGameObject:Init()
sword = Sword:Init()

-- Create new object
sword:New("Longsword", 3, "weapon", {damage = 12, weight = 4, durability = 10})
bgo:New("Red barrel", 1, "box")

-- Lets call some methods
bgo:DisplayRarity()
print()
sword:DisplayRarity()
sword:Attack()