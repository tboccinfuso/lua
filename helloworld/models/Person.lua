Person = {}

function Person:New (name, age, greeting)
  setmetatable({}, Person)

  self.__index = self
  self.name = name
  self.age = age
  self.greeting = greeting

  return self
end

function Person:Greet ()
  print(string.format("%s! %s is your name. You are %.1f years old.", self.greeting, self.name, self.age))
end

return Person